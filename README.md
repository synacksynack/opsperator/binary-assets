# Opsperator Static Assets

In an effort to make GitLab-CI reliable / after 10 retries, I can't pull the
SonarQube zip from my GitLab pipelines, despite the same curl working anywhere
else ... I've lost one day, not being able to tag a new release: let's do
something ugly/definitely bad practice, and re-locate those binary assets
in here.

| Asset                     | Source                                                                             |
| ------------------------- | ---------------------------------------------------------------------------------- |
| NotoSansCJK-Regular.ttc   | https://github.com/googlefonts/noto-cjk/raw/NotoSansV2.001/NotoSansCJK-Regular.ttc |
| get-chef-install.sh       | https://www.getchef.com/chef/install.sh                                            |
| omnitruck-chef-install.sh | https://omnitruck.chef.io/install.sh                                               |
| sonarqube-9.1.0.47736.zip | https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.1.0.47736.zip  |
| sonarqube-9.2.4.50792.zip | https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.2.4.50792.zip  |
